function createProjectObject() {

    let project = new Object();
    project.projectId = getProject_Id();
    project.ownerName = getOwner_Name();
    project.title = getTitle();
    project.category = getCategory();
    project.status = getStatus();
    project.hours = getHours();
    project.rate = getRate();
    project.description = getDescription();

    return project;
}

function deleteTableRow(event) {

    let row = event.target.parentNode.parentNode;
    let projId = row.childNodes[0].textContent;
    
    let deletion = "Do you want to delete project " + projId + "?";
    if(confirm(deletion) == true) {
        for(let i = 0; i < all_projects_arr.length; i++){
            if(all_projects_arr[i]['projectId'] == projId)
                all_projects_arr.splice(i, 1);
        }

        let tbody = document.getElementById('table_body');
        tbody.innerHTML = '';

        createTableFromArrayObjects(all_projects_arr, 'table_body');
    }
}

function editTableRow(event){

    let row = event.target.parentNode.parentNode;
    let innerText;
    let projId = row.childNodes[0].innerText;
    for(let i = 0; i < row.childNodes.length-2; i++){
        innerText = row.childNodes[i].innerText;
        row.childNodes[i].innerHTML = '<input type="text" value="' + innerText + '">';
    }
    row.childNodes[8].innerHTML = '';
    let save = document.createElement('img');
    save.src = '../images/edit.ico';
    save.addEventListener("click", function(event){
        let row = event.target.parentNode.parentNode;

        for(let i = 0; i < all_projects_arr.length; i++){
            if(all_projects_arr[i]['projectId'] == projId){
                all_projects_arr[i].projectId = row.childNodes[0].childNodes[0].value;
                all_projects_arr[i].ownerName = row.childNodes[1].childNodes[0].value;
                all_projects_arr[i].title = row.childNodes[2].childNodes[0].value;
                all_projects_arr[i].category = row.childNodes[3].childNodes[0].value;
                all_projects_arr[i].status = row.childNodes[4].childNodes[0].value;
                all_projects_arr[i].hours = row.childNodes[5].childNodes[0].value;
                all_projects_arr[i].rate = row.childNodes[6].childNodes[0].value;
                all_projects_arr[i].description = row.childNodes[7].childNodes[0].value;

                document.getElementById('table_body').innerHTML = '';
                createTableFromArrayObjects(all_projects_arr, 'table_body');
            }
        }
    });
    row.childNodes[8].appendChild(save);

}

function filterProjects(pattern){

    let result_array = all_projects_arr.filter(project => {
        for(key in project){
            if(project[key].includes(pattern))
                return true;
        }
        return false;
    });

    let tbody = document.getElementById('table_body');
    tbody.innerHTML = '';

    createTableFromArrayObjects(result_array, "table_body");
}

function updateProjectsTable() {

    all_projects_arr.push(createProjectObject());

    let tbody = document.getElementById('table_body');
    tbody.innerHTML = '';

    createTableFromArrayObjects(all_projects_arr, 'table_body');
}

function getProject_Id() {
    let p_id = document.getElementById('project_id').value;
    return p_id;
}
function getOwner_Name() {
    let o_name = document.getElementById('owner_name').value;
    return o_name;
}
function getTitle() {
    let title = document.getElementById('title').value;
    return title;
}
function getCategory() {
    let category = document.getElementById('category').value;
    return category;
}
function getHours() {
    let hours = document.getElementById('hours').value;
    return hours;
}
function getRate() {
    let rate = document.getElementById('rate').value;
    return rate;
}
function getStatus() {
    let status = document.getElementById('status').value;
    return status;
}
function getDescription() {
    let description = document.getElementById('s-desc').value;
    return description;
}
function getFilter() {
    let filter = document.getElementById('query').value;
    return filter;
}