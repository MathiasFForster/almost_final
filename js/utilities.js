let enable_disable_Button = (action_disable) => {

    let button_add = document.getElementsByTagName('button')[0];

    if(action_disable) {
        button_add.setAttribute('disabled','disabled'); 
    } else {
        button_add.removeAttribute('disabled'); 
    }
}

function set_elem_required(element_id, image) {

    let element = document.getElementById(element_id);
    element.setAttribute('required', '');

    let labels = document.getElementsByTagName('label');
    let label = null;

    for(let i = 0; i < labels.length; i++) {
        if(labels[i].htmlFor == element_id) {
            label = labels[i];
        }
    }

    label.appendChild(image);
}

function createTableFromArrayObjects(project_array, parent_id) {

    let parent = document.getElementById(parent_id);

    project_array.forEach(proj => {
        let row = document.createElement('tr');

        Object.values(proj).forEach(text => {
            let cell = document.createElement('td');
            let Txt_node = document.createTextNode(text);
            cell.appendChild(Txt_node);
            row.appendChild(cell);
        });
        
        let edit = document.createElement('td');
        img = document.createElement('img');
        img.src = '../images/edit.ico';
        img.alt = 'edit';
        img.addEventListener("click", function(event){
            editTableRow(event);
        });
        edit.appendChild(img);
        row.appendChild(edit);

        let del = document.createElement('td');
        img = document.createElement('img');
        img.src = '../images/delete.ico';
        img.alt = 'delete';
        img.addEventListener("click", function(event){
            deleteTableRow(event);
        });
        del.appendChild(img);
        row.appendChild(del);

        parent.appendChild(row);
        
    });
}

function validateElement(regex, str) {
    
    if(!regex.test(str)) {
        return false;
    } else {
        return true;
    }
}

function setElementFeedback(element_id, validation_condition, div_id) {

    let parent_div = document.getElementById(div_id);
    let message = document.createElement('p');
    message.textContent = 'wrong format for ' + element_id;

    let image_wrong = document.createElement("img");
    image_wrong.src = "../images/wrong.PNG";

    let image_valid = document.createElement("img");
    image_valid.src = "../images/valid.PNG";

    if(!validation_condition) {
        set_elem_required(element_id, image_wrong);
        parent_div.appendChild(message);
    } else {
        set_elem_required(element_id, image_valid);
    }

}

function repetition_avoidance(div_id) {
    
    if(document.getElementById(div_id).children[2] != null) {
        document.getElementById(div_id).children[2].remove();
    }
}
