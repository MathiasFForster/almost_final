"uses strict";

let all_projects_arr = [];
let regex_projId = new RegExp('^[a-zA-Z][a-zA-Z0-9/&_]{2,10}$');
let regex_ownerN = new RegExp('^[a-zA-Z][a-zA-Z0-9/]{2,10}$');
let regex_ProjT = new RegExp('[a-zA-Z0-9]{3,25}$');
let regex_NberAndRate = new RegExp('^[0-9]{1,3}$');
let regex_ProjDesc = new RegExp('^[a-zA-Z ]{3,65}$');
let validations = [true,true,true,true,true,true,true,true];

document.addEventListener('DOMContentLoaded', function() {

    enable_disable_Button(true);

    let project_id = document.getElementById('project_id');
    project_id.addEventListener("blur", validateProjId);

    let owner_name = document.getElementById('owner_name');
    owner_name.addEventListener("blur", validateOwnerName);

    let title = document.getElementById('title');
    title.addEventListener("blur", validateTitle);

    let hours = document.getElementById('hours');
    hours.addEventListener("blur", validateHours);

    let rate = document.getElementById('rate'); 
    rate.addEventListener("blur", validateRate);

    let description = document.getElementById('s-desc');
    description.addEventListener("blur", validateDesc);

    let category = document.getElementById('category');
    category.addEventListener("blur", validateCategory);
    
    let status = document.getElementById('status');
    status.addEventListener("blur", validateStatus);
    
    let button_add = document.getElementsByTagName('button')[0];
    button_add.addEventListener("click", generate_table);

    let queryFilter = document.getElementById('query');
    queryFilter.addEventListener("blur", filter);
    
});

function validateRate() {

    repetition_avoidance('sixth-div');

    if(document.getElementsByTagName('label')[5].childElementCount == 1) {
        document.getElementsByTagName('label')[5].childNodes[1].remove();
    }
    
    setElementFeedback('rate',validateElement(regex_NberAndRate,getRate()), 'sixth-div');

    if(validateElement(regex_NberAndRate,getRate())) {
        validations[0] = false;
    } else {
        validations[0] = true;
    }
    button_validation();
}
function validateDesc() {

    repetition_avoidance('short-description');

    if(document.getElementsByTagName('label')[7].childElementCount == 1) {
        document.getElementsByTagName('label')[7].childNodes[1].remove();
    }

    setElementFeedback('s-desc',validateElement(regex_ProjDesc,getDescription()), 'short-description');

    if(validateElement(regex_ProjDesc,getDescription())) {
        validations[1] = false;
    } else {
        validations[1] = true;
    }
    button_validation();
}
function validateTitle() {

    repetition_avoidance('third-div');

    if(document.getElementsByTagName('label')[2].childElementCount == 1) {
        document.getElementsByTagName('label')[2].childNodes[1].remove();
    }

    setElementFeedback('title',validateElement(regex_ProjT,getTitle()), 'third-div');

    if(validateElement(regex_ProjT,getTitle())) {
        validations[2] = false;
    } else {

        validations[2] = true;
    }
    button_validation(); 
}
function validateHours() {

    repetition_avoidance('fifth-div');

    if(document.getElementsByTagName('label')[4].childElementCount == 1) {
        document.getElementsByTagName('label')[4].childNodes[1].remove();
    }

    setElementFeedback('hours',validateElement(regex_NberAndRate,getHours()), 'fifth-div');

    if(validateElement(regex_NberAndRate,getHours())) {
        validations[3] = false;
    } else {
        validations[3] = true;
    }
    button_validation();
}
function validateOwnerName() {

    repetition_avoidance('second-div');

    if(document.getElementsByTagName('label')[1].childElementCount == 1) {
        document.getElementsByTagName('label')[1].childNodes[1].remove();
    }

    setElementFeedback('owner_name',validateElement(regex_ownerN, getOwner_Name()), 'second-div');

    if(validateElement(regex_ownerN, getOwner_Name())) {
        validations[4] = false;
    } else {
        validations[4] = true;
    }
    button_validation();
}
function validateProjId() {

    repetition_avoidance('first-div');

    if(document.getElementsByTagName('label')[0].childElementCount == 1) {
        document.getElementsByTagName('label')[0].childNodes[1].remove();
    }

    setElementFeedback('project_id',validateElement(regex_projId,getProject_Id()), 'first-div');

    if(validateElement(regex_projId,getProject_Id())) {
        validations[5] = false;
    } else {
        validations[5] = true;
    }
    button_validation();
}

function validateCategory() {

    repetition_avoidance('fourth-div');

    if(document.getElementsByTagName('label')[3].childElementCount == 1) {
        document.getElementsByTagName('label')[3].childNodes[1].remove();
    }

    if(getCategory() == 'practical' || getCategory() == 'theoretical' || getCategory() == 'fundamental research' || getCategory() == 'empirical') {
        setElementFeedback('category', true, 'fourth-div');
        validations[6] = false;
    } else {
        setElementFeedback('category', false, 'fourth-div');
        validations[6] = true;
    }
    button_validation();
}
function validateStatus() {

    repetition_avoidance('seventh-div');

    if(document.getElementsByTagName('label')[6].childElementCount == 1) {
        document.getElementsByTagName('label')[6].childNodes[1].remove();
    }

    if(getStatus() == 'completed' || getStatus() == 'ongoing' || getStatus() == 'planned') {
        setElementFeedback('status', true, 'seventh-div');
        validations[7] = false;
    } else {
        setElementFeedback('status', false, 'seventh-div');
        validations[7] = true;
    }

    button_validation();
}
function button_validation() {

    enable_disable_Button(false);

    for(let i = 0; i < validations.length; i++) {

        if(validations[i]) {
            enable_disable_Button(true);
            break;
        }
    }
}

function generate_table() {

    updateProjectsTable();
}
function filter() { 

    filterProjects(getFilter());
}
 

